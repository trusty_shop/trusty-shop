/**
 * Picture.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'pictures',
  meta: {
    schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {

    b64: {
      type: 'string'
    },

    good: {
      model: 'good',
      columnName: 'good_id'
    }
  },
  //------------------------------------METHODES------------------------------------
  getPicturesOf: function(goodID){
    return Picture.find({good: goodID});
  },

  getOnePictureOf: function(goodID){
    return Picture.findOne({where : {good: goodID}, limit:1});
  },

  getPicturesOfSeveralObjects: function(goods){
    var pictures = [];
    for(var i = 0; i < goods.length; i++){
      pictures.push = Picture.findOne({good: goods[i].id});
    }
    return pictures;
  },

  addPicture: function(picture, goodID){
    return Picture.create({b64:picture,good:goodID}).then();
  }
};

