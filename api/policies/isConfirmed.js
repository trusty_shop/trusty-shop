
/**
 * isConfirmed
 *
 * @module      :: Policy
 * @description :: Simple policy to check if user is confirmed
 *                 Assumes that your login action in one of your controllers sets `req.session.user.confirmed = true;`
 * @author :: Ibrahim Mourade
 *
 */
module.exports = function(req, res, next) {
    
      // User is allowed, proceed to the next policy, 
      // or if this is the last policy, the controller
      if (req.session.user.confirmed) {
        return next();
      }
    
      // User is not allowed
      // (default res.forbidden() behavior can be overridden in `config/403.js`)
      return res.view('403', { data: 'Votre compte n\'est pas encore validé.'});
      //return res.forbidden('Vous n\'etes pas confirmer');
    };
    