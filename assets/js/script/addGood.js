$(document).ready(function () {


  var dateMin = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
  var dateMax = new Date(dateMin.getTime() + 7 * 24 * 60 * 60 * 1000);

  $("#date").attr("min", formatDate(dateMin));
  $("#date").attr("max", formatDate(dateMax));

  $("#prog").hide();

  //Récupération dans un tableau de toute les images
  var files;
  var uploadArray = [];
  $("#file_image").on('change', function () {
    files = $(this).get(0).files;
    uploadArray = [];
    if (uploadArray.length + files.length > 5) {
      alert("Vous ne pouvez uploader que 5 fichiers");
    } else {
      if (files.length > 0) {
        for (var i = 0; i < files.length; i++) {
          uploadArray.push(files[i]);
        }
      }
    }
  });//Fin de récupération  

  $("#updateGood").on('click', function () {
    $("#messageAddGood").html('');
    document.getElementById("progressBar").style.width = "0%";
    var title = $.trim($("#title").val());
    var category = $("#category option:selected");
    var price = $("#price").val();
    var description = $("#description").val();
    var date = $("#date").val();
    var idGood = $("#idGood").val();
    var error = 0;
    if ($.trim(title).length == 0) {
      $("#messageAddGood").append('<div class="alert alert-danger">Vous devez remplir le champ titre</div>');
      error++;
    }
    if (price.length == 0) {
      $("#messageAddGood").append('<div class="alert alert-danger">Vous devez remplir le champ prix</div>');
      error++;
    }
    if (!category.is(":enabled")) {
      $("#messageAddGood").append('<div class="alert alert-danger">Vous devez choisir une catégorie</div>');
      error++;
    }

    if (error == 0){
      $("#messageAddGood").html('');
      updateGood(title, description, date, price, category.text().trim(), uploadArray, idGood);
    }


  });


  //Cliquer sur boutton pour confirmer l'ajout de l'article /addGood 
  $("#confirm").click(function () {
    $("#messageAddGood").html('');
    document.getElementById("progressBar").style.width = "0%";
    var title = $("#title").val().trim();
    
    var category = $("#category option:selected");
    var otherCategory = $("#otherCategory").val();
    var price = $("#price").val();
    var description = $("#description").val();
    var categoryFinal = "";
    var date = $("#date").val();
    var error = 0;
    if ($.trim(title).length == 0) {
      $("#messageAddGood").append('<div class="alert alert-danger">Veuillez remplir le champ titre</div>');
      error++;
    }
    if (price.length == 0) {
      $("#messageAddGood").append('<div class="alert alert-danger">Vous devez remplir le champ prix</div>');
      error++;
    }
    if ($.trim(otherCategory).length != 0) {
      categoryFinal = otherCategory;
    } else if (category.is(":enabled")) {
      categoryFinal = category.text().trim();
    } else {
      $("#messageAddGood").append('<div class="alert alert-danger">Vous devez remplir un des deux champs catégories</div>');
      error++;
    }

    if (!$('input[name=checkbox]').prop('checked')) {
      $("#messageAddGood").append('<div class="alert alert-danger">Veuillez accepter les termes et les conditions général du site.</div>');
      error++;
    }
    if (error == 0) {
      $("#messageAddGood").html('');
      updateGood(title, description, date, price, categoryFinal, uploadArray);
    } 

  });//Fin de l'ajout de l'article
});

function imgToB64(file) {
  return new Promise(function (resolve, reject) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () { resolve(reader.result) };
    reader.onerror = function (error) { reject(error) };
  });
}


function fillPicturesArray(b_64) {
  return b_64;
}

function updateGood(title, description, end_date, price, category, uploadArray, idGood) {
  var path = (typeof idGood === 'undefined') ? "/addGood" : "/updateGood";
  var picturesArray = [];
  var promises = [];
  uploadArray.forEach(function (p) {
    promises.push(imgToB64(p).then(function (data) {
      progress(100 / (uploadArray.length + 1));
      picturesArray.push(data);
    }));
  });

  Promise.all(promises).then(function () {
    $.ajax({
      url: path,
      type: "POST",
      data:
        {
          title: title,
          price: price,
          description: description,
          end_date: end_date,
          picturesArray: picturesArray,
          category: category,
          idGood: idGood
        }
      ,
      success: function (res) {

        document.getElementById("progressBar").style.width = "100%";
        $("#file_image").val("");
        $("#messageAddGood").html('<div class="alert alert-success">Votre bien à été correctement traité</div>')
      },
      error: function (err) {
        $("#messageAddGood").html('<div class="alert alert-danger">Erreur lors du traitement Veuillez réessayer</div>')

      }
    });
  });
}

function progress(step) {
  $("#prog").show();
  var elem = document.getElementById("progressBar");
  var width = elem.style.width;
  width = width.replace("%", "");
  elem.style.width = (parseInt(width) + step) + '%';
}

function formatDate(date) {
  var dd = date.getDate();
  var mm = date.getMonth() + 1;
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  return date.getFullYear() + "-" + mm + "-" + dd;
}



