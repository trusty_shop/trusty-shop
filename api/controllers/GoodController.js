/**
 * GoodController
 *
 * @description :: Server-side logic for managing Goods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    //------------------------!!!!!!!!!!!-----------TODO buyGood---------------------!!!!!!!!!!!!------------------
    getAll: function (req, res) {
        //TODO
        Good.getAll().then(function (results) {
            if (!results) {
                res.view('404');
            } else {
                res.ok(JSON.stringify(results));
            }
        }).catch(function () {
            res.view('500');
        });
    },

    getById: function (req, res) {
        var id = req.param('id');
        var good;
        var auctions;
        var moment = require('moment');
        var comment;
        var user;
        Good.getById(id).then(function (result) {
            good = result
            if(!good)
                return undefined;
            return User.getUserById(good.id_seller);
        }).then(function (u){
            if(!u)
                return undefined;
            user = u;
            return Auction.getAllAuctions(id);
        }).then(function (results) {
            if(!good) {
                return undefined;
            }else {
                auctions = results
                return Comment.getByGood(good.id);
            }
        }).then(function(c) {
            if(!good) {
                return undefined;
            }else {
                comment = c;
                return Picture.getPicturesOf(good.id);
            }
        }).then(function (pictures) {
            if (!pictures) {
                return res.view('404');
            } else{
                if (req.session.user) {
                    if(req.session.user.id  == good.id_seller){
                        Category.getAllCategories().then(
                            (categories) => {
                                User.getUserById(good.id_buyer).then(function(b){
                                    return res.view('mygood',{good: good, pictures : pictures, auctions: auctions, moment:moment, categories: categories, comment: comment, buyer:b});
                                });
                            }
                        );
                    } else if(req.session.user.id == good.id_buyer) {
                        if(comment)
                            return res.view('good',{good: good, pictures : pictures, auctions: auctions, moment:moment, canComment: false, seller: user, comment: comment});
                        return res.view('good',{good: good, pictures : pictures, auctions: auctions, moment:moment, canComment: true, seller: user, comment: comment});
                    } else {
                        return res.view('good',{good: good, pictures : pictures, auctions: auctions, moment:moment, canComment:false, comment: comment, seller: user});
                    }
                } else {
                    return res.view('good',{good: good, pictures : pictures, auctions: auctions, moment:moment, canComment:false, comment: comment, seller: user});
                }
            }
        }).catch(function (err) {
            console.log(err);
            res.view('500');
        });
    },

    getGoodsByCategory: function (req, res) {
        var category = req.param('categorie');
        var goods;
        var moment = require('moment');

        function fillGood(g){
            return new Promise(
                (resolve, reject) => {
                    Picture.getOnePictureOf(g.id).then(
                        (p) => {
                            g.picture = p;
                            resolve(g);
                        }
                    );
                }
            );
        }

        Category.getCategoryByName(category).then(function (categoryFound) {
            if (!categoryFound) {
                res.view('404');
            } else {
                category = categoryFound;
                Good.getByCategory(categoryFound.id).then(function (goodsFound) {
                    if (!goodsFound) {
                        return res.view('404');
                    } else {
                        goods = goodsFound;
                        
                        var promises = [];
                        
                        goods.map(g => {
                            promises.push(fillGood(g).then(
                                function(good) {
                                    return good;
                                }
                            ));
                        });
                        Promise.all(promises).then(
                            () => {
                                return res.view('goods', { goods: goods, categorie: category.name, moment: moment});
                            }
                        );

                    }
                }).catch(function (err) {
                    console.log(err);
                    return res.view('500');
                });
            }
        })
    },

    getOnSale: function (req, res) {
        var id = req.param('id');
        var goodsOnSale;
        var moment = require("moment");

        function fillGood(g){
            return new Promise(
                (resolve, reject) => {
                    Picture.getOnePictureOf(g.id).then(
                        (p) => {
                            g.picture = p;
                            resolve(g);
                        }
                    );
                }
            );
        }

        Good.getOnSale().then(function (goods) {
            if (!goods) {
                res.view('404');
            } else {
                var promises = [];

                goods.map(g => {
                    promises.push(fillGood(g).then(
                        function(good) {
                            return good;
                        }
                    ));
                });
                Promise.all(promises).then(
                    () => {
                        return res.view('goods', { goods: goods, categorie: "Toutes les catégories", moment: moment});
                    }
                );
            }
        }).catch(function (err) {
            return res.serverError(err);
        });
    },

    //je pense que ceci ne devrait pas etre dans un controller pck
    // ca ne sera pas utilisé par quelqu'un directement mais plutot par le backend
    // TODO transformer en service !!!
    changeState: function (req, res) {
        const goodID = req.param('goodID');
        const newState = req.param('goodState');
        Good.changeState(goodID, newState).then(function (good) {
            if (!good) {
                res.view('404');
            } else {
                res.view('good', { good: good });
            }
        }).catch(function () {
            res.view('500');
        });
    },

    getByName: function (req, res) {
        var name = req.param('name')
        var goods;
        var moment = require('moment');
        
        function fillGood(g){
            return new Promise(
                (resolve, reject) => {
                    Picture.getOnePictureOf(g.id).then(
                        (p) => {
                            g.picture = p;
                            resolve(g);
                        }
                    );
                }
            );
        }
        Good.getByName(name).then(function (goodsFound) {
            if (!goodsFound) {
                return res.view('404');
            } else {
                goods = goodsFound;
                console.log(goodsFound);

                var promises = [];
                
                goods.map(g => {
                    promises.push(fillGood(g).then(
                        function(good) {
                            return good;
                        }
                    ));
                });

                Promise.all(promises).then(
                    () => {
                        return res.view('goods', { goods: goods, categorie: "Critère : " + name, moment: moment})
                    }
                );
                
            }
        }).catch(function (err) {
            console.log(err);
            res.view('500');
        });
    },

    getByPriceRange: function (req, res) {
        var min = req.param('min');
        var max = req.param('max');
        var moment = require('moment');

        var goods;
        if (min < 0 || max < 0 || max < min) {
            res.badRequest("Le minimum et maximum doivent être positifs.");
        } else {
            Good.getByPriceRange(min, max).then(function (results) {
                if (!results) {
                    res.view('404');
                } else {
                    goods = results;
                    return Picture.getPicturesOfSeveralObjects(goods);
                }
            }).then(function (pictures) {
                if (!pictures) {
                    res.view('404');
                } else {
                    res.view('goods', { goods: goods, categorie: "Toutes les catégories", pictures: pictures, moment: moment});
                }
            }).catch(function () {
                res.view('500');
            });
        }
    },

    addGood: function (req, res) {
        let title = req.body.title;
        let description = req.body.description;
        let price = req.body.price;
        let category = req.body.category;
        let picturesArray = req.body.picturesArray;
        let end_date = req.body.end_date;
        Good.addGood(title, description, end_date, req.session.user.id, price, category, picturesArray);
        return res.ok();
    },

    updateGood: function (req, res) {
        let title = req.body.title;
        let description = req.body.description;
        let price = req.body.price;
        let category = req.body.category;
        let picturesArray = req.body.picturesArray;
        let end_date = req.body.end_date;
        let idGood = req.body.idGood;
        Good.updateGood(title, description, end_date, req.session.user.id, price, category, picturesArray, idGood);
        return res.ok();
    },

    getGoodsByOwner: function (req, res) {
        var moment = require('moment');

        function fillGood(g){
            return new Promise(
                (resolve, reject) => {
                    Picture.getOnePictureOf(g.id).then(
                        (p) => {
                            g.picture = p;
                            resolve(g);
                        }
                    );
                }
            );
        }

        Good.getGoodsByOwner(req.session.user.id).then(function (goods) {
            if (!goods) {
                res.view('404');
            } else {
                
                var promises = [];
                
                goods.map(g => {
                    promises.push(fillGood(g).then(
                        function(good) {
                            return good;
                        }
                    ));
                });
                Promise.all(promises).then(
                    () => {
                        return res.view('goods', { goods: goods, moment: moment, categorie: "Mes Objets"});
                    }
                );
            }
        }).catch(function (err) {
            console.log(err);
            return res.view('500');
        });
    }
};
