/**
 * Good.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'goods',
  meta: {
    schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {

    title: {
      type: 'string'
      //add size
    },

    description: {
      type: 'string'
    },

    id_seller: {
      model: 'user',
      columnName: 'seller_id',
      required: true
    },

    id_buyer: {
      model: 'user',
      columnName: 'buyer_id'
    },

    starting_price: {
      type: 'integer'
    },

    category_id: {
      model: 'category',
      columnName: 'category_id'
    },

    good_state: {
      type: 'string',
      enum: ['S', 'O', 'P', 'D', 'U'],
      defaultsTo: 'O',
    },

    end_date: {
      type: 'date'
    },

    start_date: {
      type: 'date',
      defaultsTo: function () {
        return new Date();
      }
    }
  },

  //------------------------------------METHODES------------------------------------
  /**
   * Affiche tous les goods
   */
  getAll: function () {
    return Good.find({"sort":"end_date ASC"});
  },

  getById: function (id) {
    return Good.findOne({ id: id });
  },

  getOnSale: function () {
    return Good.find({ good_state: 'O' , sort:'end_date ASC'});
  },

  changeState: function (goodID, newState) {

      Good.update({ id: goodID }, { good_state: newState }).exec(function afterwards(err, updated){
        //console.log(updated[0].good_state);
      });
  },

  addBuyer: function (auction) {
    var id_good = auction.id_good;
    var id_buyer = auction.id_user;
    //console.log(id_good + " et " + id_buyer);
    Good.update({ id: id_good }, { id_buyer: id_buyer }).exec(function afterwards(err, updated) {
      //  console.log(updated[0].id_buyer);
    });

  },

  getByName: function (name) {
    return Good.find({ title: { 'like': '%'+name+'%'}, good_state: 'O', "sort":"end_date ASC" });
  },

  getEndDateByDay: function(date) {
    return Good.find({ end_date: date });
  },
  
  getLastGoodsToClose: function(date) {
    return Good.find({ end_date: date, good_state: 'O' });
  },

  getByPriceRange: function (min, max) {
    return Good.find({ starting_price: { '>': min, '<': max }, "sort":"end_date ASC"});
  },

  getByCategory: function (categoryID) {
    return Good.find({ category_id: categoryID, good_state:'O', sort:'end_date ASC'});
  },

  addGood: function (title, description, end_date, id_seller, price, category, picturesArray) {
    Category.findOrCreate({ name: category }, { name: category }).then(function (cat) {
      Good.create({ title: title, description: description, end_date: end_date, id_seller: id_seller, category_id: cat, starting_price: price }).then(function (good) {
        goodCreated = good;
        picturesArray.forEach(picture => {
            Picture.addPicture(picture,goodCreated.id);
        });
      });
    }).then(function(goodCreated){
      return goodCreated;
    }).catch(function(err){
      console.log(err);
    });
  },

  updateGood: function (title, description, end_date, id_seller, price, category, picturesArray, idGood) {
    Category.findOrCreate({ name: category }, { name: category }).then(function (cat) {
      Good.update({id:idGood}, { title: title, description: description, category_id: cat}).then(function (good) {
        picturesArray.forEach(picture => {
            Picture.addPicture(picture,idGood);
        });
      });
    }).then(function(goodUpdated){
      return goodUpdated;
    }).catch(function(err){
      console.log(err);
    });
  },

  getGoodsByOwner: function (id) {
    return Good.find({ id_seller: id, "sort":"end_date ASC"});
  },
}
