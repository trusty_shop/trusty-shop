/**
 * Tag.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'tags',
  meta: {
     schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {
   
    name: {
      type: 'string'
    },

    id_good: {
      model:'good',
      columnName: 'good_id'
    }
  },
  //------------------------------------METHODES------------------------------------
  getObjectTags: function(goodID){
    return Tag.find({id_good: goodID});
  },

  addTagToObject: function(goodID, tagName){
    return Tag.create({name: tagName, id_good: goodID});
  }
};

