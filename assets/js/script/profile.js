$(document).ready(function(){


    //On modifie seulement le téléphone car tout le reste des champs seront disabled
    $("#updateUser").on('click',function(){
        var phone = $("#phone").val();
        if(phone.length==0){
            console.log("Le champ numéro de téléphone est vide");
        }else{
            updateUserData(phone);
        }
    });
    
});

function updateUserData(p, e){

    $.ajax({
        url:"/profileUpdate",
        type: "POST",
        data:{phone : p},
        success:function(res){
            //OK
        },
        error: function(err){
            console.log(JSON.stringify(err));
            console.log("Une erreur s'est produite dans la méthode updateUserData() dans profile.js");
        }
    })

}