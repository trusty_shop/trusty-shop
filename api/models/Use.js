/**
 * Use
 *
 * @module      :: Model
 * @description :: Tracks the usage of a given Jwt
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {
  meta: {
    schemaName: 'trusty_db'
  },
  attributes: require('waterlock').models.use.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  })
};
