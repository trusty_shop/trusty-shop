$(document).ready(function(){
    /*NOTRE APPLICATION*/

    
    

    //Pour effectuer une enchère sur un article
    $("#auction").on('click',function (){
      var url  = window.location.pathname;
      var id = url.substring(url.lastIndexOf('/') + 1);
      var montant = $('#montant').val();
      auction(id, montant);
    });//fin enchère
    
    //Boutton de recherche /search
    $("#search").click(function(){
      var search_element = $.trim($("#search_element").val());

      if(search_element.length!=0){
        window.location.href = '/search/'+search_element;
      }
    });//fin de recherche
  



    //Cliquer sur bouton Envoyer , pour envoyer le mdp sur mail
    $("#lost_password").submit(function(event){
      event.preventDefault();
      var email = $.trim($("#email_password_lost").val());
      if(email.length!=0)
        sendMailForPassword(email);
    });//fin authentification


   

    $("#commentButton").click(function(){
      var score = $('input[name=score]:checked').val();
      var comment = $("#comment").val();
      var idGood = $("#idGood").val();
      var userID = $("#userID").val();
      if($.trim(comment).length==0){
        console.log("Le commentaire doit être complété");
      }
      else commentGood(userID, score,comment,idGood);
    });  
    
});

function commentGood(userID, score,comment,idGood){
  $.ajax({
    url: "/comment",
    method: "POST",
    dataType: "JSON",
    data: {userID: userID, idGood: idGood, score : score,comment:comment},
    success: function (res) {
        console.log("success");
    },
    error: function (err) {
    }
  });
}

function auction(idGood, montant) {
  $.ajax({
      url: "/addAuction",
      method: "POST",
      data: {id: idGood, price : montant},
      success: function (data) {
        console.log(data);
        $("#errorAuction").html(
          '<div class="alert alert-success">'+data+'</div>'
        );
      },
      error: function (data) {
        
        $("#errorAuction").html(
          '<div class="alert alert-danger">'+data.responseText+'</div>'
        );
        
        console.log(data);
      }
  });
}




function sendMailForPassword(email){
  $.ajax({
    url:"/authentication",
    type:"POST",
    dataType:"json",
    data:{email:email},
    success:function(res){
      var result = JSON.parse(res);
      if(!result){
        $(".error").html("Votre compte n'existe pas !");
      }else{
        $(".error").css("color","green");
        $(".error").html("Un mail vous a été envoyé avec votre mot de passe");
      }
    },
    error: function(err){
      console.log("Une erreur s'est produite dans l'ajax de la méthode sendMailPassword() dans le script.js")
    }
  })
}
