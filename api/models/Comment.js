/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'comments',
  meta: {
     schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {
    id_user_commented: {
      model:'user',
      columnName: 'user_commented_id',
      required:true
    },

    id_author: {
      model:'user',
      columnName: 'author_id'
    },

    id_good: {
      model:'good',
      columnName: 'good_id'
    },

    comment: {
      type: 'string'
    },

    score: {
      type: 'integer',
      enum : [0,1,2,3,4,5],
      defaultsTo : 0
    }
  },
  //------------------------------------METHODES------------------------------------
  getAllCommentsByGoodId: function(goodID){
    return Comment.find({id_good: goodID}).populate('id_author');
  },

  getAllCommentsByUserCommented: function(userID){
    return Comment.find({user_commented_id: userID}).populate('id_author');
  },

  getAllCommentsByAuthorId: function(authorID){
    return Comment.find({author_id: authorID}).populate('id_author');
  },

  getAllCommentsForGoodByScore: function(goodID,score){
    return Comment.find({id_good: goodID, score: score}).populate('id_author');
  },

  getByGood: function(goodID){
    return Comment.findOne({id_good: goodID});
  },

  addComment: function(userID, authorID, goodID, comment, score){
    return Comment.create({id_user_commented: userID, author_id: authorID, good_id: goodID, comment: comment, score: score});
  }
};

