const schedule = require('node-schedule');

module.exports = {
    // Récupère tous les biens du jour
    getGoodsByDay: function() {
      var day = new Date();

      Good.getEndDateByDay(day).then(function(goods) {
        ScheduleService.checkEndSale(goods);
      }).catch(function(err){
        console.log("getGoodsByDay : Error");
        console.log(err);
      });
    },

    //
    checkEndSale: function(goods) {
      // S'il y a des biens dont la vente se clôture
      if(goods) {
        // Parcours du tableau des biens
        for(var i = 0; i < goods.length; i++) {
          var id = goods[i].id;
          // On récupère la dernière enchère si elle existe
          Auction.getLastAuction(id).then(function(auction) {
              if(auction){
                console.log("Vendu");
                Good.changeState(auction.id_good, 'S');
                Good.addBuyer(auction);
              }

          }).catch(function(err) {
            console.log("CheckEndSale : Error");
            console.log(err);
          });
        }
      } else {
        console.log("Rendez-vous à la prochaine clôture");
      }
    },

    getNotClosedGoods: function() {
      var day = new Date();

      Good.getLastGoodsToClose(day).then(function(goods) {
        ScheduleService.closeLastGoods(goods);
      }).catch(function(err){
        console.log("getLastGoodsToClose : Error");
        console.log(err);
      });
    },

    // Finition des clôtures
    closeLastGoods: function(goods) {
      if(goods) {
        // Parcours du tableau des biens restants
        for(var i = 0; i < goods.length; i++) {
            console.log("Invendu");
            Good.changeState(goods[i].id, 'U');
        }
      }
    },

    // Premier scheduler pour la clôture des ventes qui ont au moins une enchère
    jobToDo: function() {
      var j = schedule.scheduleJob('1 30 20 * * *', function() {
        ScheduleService.getGoodsByDay();
      });
    },

    // Deuxième enchère pour la clôture des ventes sans enchères
    jobToDoBis: function() {
      var j = schedule.scheduleJob('2 30 20 * * *', function() {
        ScheduleService.getNotClosedGoods();
      });
    }
}

module.exports.jobToDo();
module.exports.jobToDoBis();
