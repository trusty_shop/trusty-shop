/**
 * CommentController
 *
 * @description :: Server-side logic for managing Comments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getAllCommentsByGoodId: function(req, res){
        var goodID = req.param('goodID');
        Comment.getAllCommentsByGoodId(goodID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getAllCommentsByUserCommented: function(req, res){
        var userID = req.params.id;
        Comment.getAllCommentsByUserCommented(userID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                //TODO
            }
        }).catch(function(){
            res.view('500');
        })
    },

    getAllCommentsByAuthorId: function(req, res){
        var authorID = req.param('authorID');
        Comment.getAllCommentsByAuthorId(authorID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getAllCommentsForGoodByScore: function(req, res){
        var goodID = req.param('goodID');
        var score = req.param('score');
        if(score < 1 || score > 5){
            //Renvoie code 400 - front affiche le msg
            res.badRequest("Le score ne peut pas être inférieur à 1 et supérieur à 5.");
        }else{
            Comment.getAllCommentsForGoodByScore(goodID, score).then(function(results){
                if(!results){
                    res.view('404');
                }else{
                    res.ok(JSON.stringify(results));
                }
            }).catch(function(){
                res.view('500');
            }); 
        }
    },

    addComment: function(req, res){
        var userID = req.param('userID');
        var goodID = req.param('idGood');
        var comment = req.param('comment');
        var score = req.param('score');
        if(score < 1 || score > 5){
            //Renvoie code 400 - front affiche le msg
            res.badRequest("Le score ne peut pas être inférieur à 1 et supérieur à 5.");
        }else{
            Comment.addComment(userID, req.session.user.id, goodID, comment, score).then(function(results){
                if(!results){
                    return res.view('404');
                }else{
                    return res.ok(JSON.stringify(results));
                }
            }).catch(function(err){
                console.log(err);
                return res.view('500');
            })
        }
    }
};

