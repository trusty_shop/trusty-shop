/**
 * Auction.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'auctions',
  meta: {
     schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {

    id_good: {
     model:'good',
     columnName:'good_id'
    },

    id_user: {
      model:'user',
      columnName:'author_id'
    },

    price: {
      type: 'float'
    },

  },
  //------------------------------------METHODES------------------------------------
  //Récupère la dernière auction (=la plus récente)
	getLastAuction: function(goodID){
    return Auction.findOne({id_good: goodID, sort:'createdAt DESC' });
  },

  getAuctionsByUser: function(userID){
    return Auction.find({id_user: userID});
  },

  getAllAuctions: function(goodID){
    return Auction.find({id_good: goodID, sort:'createdAt DESC'}).populate('id_user');
  },

  //Ajoute une nouvelle auction à un objet
  addAuction: function(goodID, userID, price){
    //TODO checks
    return Auction.create({id_good: goodID, id_user: userID, price: price}).then();
  }
};
