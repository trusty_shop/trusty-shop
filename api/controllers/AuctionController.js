/**
 * AuctionController
 *
 * @description :: Server-side logic for managing Auctions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    auction: function(req, res){
        var id = req.param("id");
        Auction.create({author_id:req.session.user.id}, {price:100}, {id_good:id}).then(function(auction){
            sails.sockets.broadcast('auctions'+id, auction);
        });
    },

    getLastAuction: function(req, res){
        const id = req.param('id');
        Auction.getLastAuction(id).then(function (results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getAllAuctions: function(req, res){
        const goodID = req.param('id');
        Auction.getAllAuctions(goodID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        })
    },

    getAuctionsByUser: function(req, res){
        const userID = req.param('id');
        var moment = require("moment");
        var auctions = [];
        var invalidAuctions = [];
        var failedAuctions = [];
        var successAuctions = [];
        var validAuctions = [];
        
        function sort(auction){
            return new Promise((resolve, reject)=>{
                Good.getById(auction.id_good).then((g)=>{
                    auction.good = g;
                    if(g.good_state != 'O'){
                        Auction.getLastAuction(auction.id_good).then((lastAuct)=>{
                            console.log(lastAuct);
                            if(lastAuct.id === auction.id){
                                successAuctions.push(auction);
                                resolve();
                            }else{
                                failedAuctions.push(auction);
                                resolve();
                            }
                        });  
                    }else{
                        Auction.getLastAuction(auction.id_good).then((lastAuct)=>{
                            console.log(lastAuct);
                            if(lastAuct.id === auction.id){
                                validAuctions.push(auction);
                                resolve();
                            }else{
                                invalidAuctions.push(auction);
                                resolve();
                            }
                        });  
                    }
                });
            });
        }

        Auction.getAuctionsByUser(req.session.user.id).then(function(results){
            if(!results){
                res.view('404');
            }else{
                var promises = []
                results.forEach(element => {
                    promises.push(sort(element));
                });
                Promise.all(promises).then( () => {
                        return res.view('myauctions', {
                            types :["warning","primary","success","danger"],
                            titles:["en cours et invalides", "en cours et valides", "réussies", "échouées"],
                            auctions:[invalidAuctions,validAuctions,successAuctions,failedAuctions],
                            moment: moment
                        });
                    }
                )
            }
        }).catch(function(){
            res.view('500');
        })
    },

    addAuction: function(req, res){
        let goodID = req.body.id;
        let userID = req.session.user.id;
        let price = req.body.price;
        //Récupère dernière enchère pour comparer prix
        Auction.getLastAuction(goodID).then(function(auction){
            //Si pas encore d'enchere
            console.log(auction);
            if(!auction){
                
                Good.getById(goodID).then( (g) => {
                    if(g.starting_price > price){
                        return res.badRequest("Le prix de l' enchère doit être d'au moins "+g.starting_price);
                    }else{
                        Auction.addAuction(goodID, userID, price);
                        return res.ok("Votre enchère à été prise en compte");
                    }
                });
            }else{
                //Vérifie prix > prix dernière enchère
                if(auction.price >= price){
                    //retourne un 400 - front doit afficher le msg erreur
                    return res.badRequest("Le prix de la nouvelle enchère doit être supérieur à celui de la dernière enchère.");
                }else{
                    Auction.addAuction(goodID, userID, price).then( () => {
                        return res.ok("Votre enchère à été prise en compte");
                    } );
                }
            }
        }).catch(function (err){
            console.log(err);
            res.view('500');
        });
    }
};

