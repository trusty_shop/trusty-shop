/**
 * Category.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'categories',
  meta: {
    schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {

    name: {
      type: 'string',
      unique: true,
    },

    description: {
      type: 'string'
    }
  },
  //------------------------------------METHODES------------------------------------
  getAllCategories: function () {
    return Category.find();
  },

  getCategoryById: function (categoryID) {
    return Category.find({ id: categoryID });
  },

  getCategoryByName: function (name) {
    return Category.findOne({ name: name });
  }

};

